#include <iostream>

#include "Array2d.hpp"
#include "Conv2d.hpp"

int main()
{
    Array2d<float> image(3, 3);

    image[0][0] = 1;
    image[0][1] = 2;
    image[0][2] = 3;

    image[1][0] = 4;
    image[1][1] = 5;
    image[1][2] = 6;

    image[2][0] = 7;
    image[2][1] = 8;
    image[2][2] = 9;

    Array2d<float> kernel(3, 3);

    kernel[0][0] = -1;
    kernel[0][1] = -2;
    kernel[0][2] = -1;

    kernel[1][0] = 0;
    kernel[1][1] = 0;
    kernel[1][2] = 0;

    kernel[2][0] = 1;
    kernel[2][1] = 2;
    kernel[2][2] = 1;

    Array2d<float> result = Conv2d(image, kernel);

    return 0;
}