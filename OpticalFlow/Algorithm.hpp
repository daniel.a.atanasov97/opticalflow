#pragma once

template <typename Type>
Type Min(Type lhs, Type rhs)
{
    if (lhs < rhs)
    {
        return lhs;
    }
    else
    {
        return rhs;
    }
}

template <typename Type, typename ...Types>
Type Min(Type value, Types... other)
{
    return Min(value, Min(other...));
}

template <typename Type>
Type Max(Type lhs, Type rhs)
{
    if (lhs > rhs)
    {
        return lhs;
    }
    else
    {
        return rhs;
    }
}

template <typename Type, typename ...Types>
Type Max(Type value, Types... other)
{
    return Max(value, Max(other...));
}

template <typename InputIterator, typename OutputIterator>
OutputIterator Copy(
    InputIterator begin,
    InputIterator end,
    OutputIterator dest
)
{
    while (begin != end)
    {
        *dest++ = *begin++;
    }

    return dest;
}