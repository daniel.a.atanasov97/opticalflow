#pragma once

#include "Vector.hpp"

template<typename ValueType, typename SizeType = int>
struct BufferView
{
public:
    using Const = BufferView<ValueType const, SizeType>;

private:
    ValueType *data_;
    SizeType size_;

public:
    BufferView() :
        data_(nullptr),
        size_(0)
    {

    }

    BufferView(ValueType *data, SizeType size) :
        data_(data),
        size_(size)
    {

    }

    ValueType const *Data() const
    {
        return data_;
    }

    SizeType Size() const
    {
        return size_;
    }

    ValueType const &operator[](int idx) const
    {
        return data_[idx];
    }

    ValueType &operator[](int idx)
    {
        return data_[idx];
    }

    operator Const()
    {
        return Const(data_, size_);
    }
};

template <typename ValueType, typename SizeType = int>
using ConstBufferView = typename BufferView<ValueType, SizeType>::Const;

template <typename ValueType, typename SizeType = int>
struct Array2d
{
public:
    using ConstRow = ConstBufferView<ValueType, SizeType>;
    using Row = BufferView<ValueType, SizeType>;

private:
    Vector<ValueType, SizeType> buffer_;

    SizeType width_;
    SizeType height_;

public:
    Array2d() :
        width_(0),
        height_(0)
    {

    }

    Array2d(SizeType width, SizeType height)
    {
        Resize(width, height);
    }

    SizeType Width() const
    {
        return width_;
    }

    SizeType Height() const
    {
        return height_;
    }

    SizeType Size() const
    {
        return Width() * Height();
    }

    void Resize(SizeType width, SizeType height)
    {
        width_ = width;
        height_ = height;

        buffer_.Resize(Size());
    }

    ConstBufferView<ValueType, SizeType> operator[](int idx) const
    {
        SizeType width = Width();
        return ConstBufferView<ValueType, SizeType>(&buffer_[idx * width], width);
    }

    BufferView<ValueType, SizeType> operator[](int idx)
    {
        SizeType width = Width();
        return BufferView<ValueType, SizeType>(&buffer_[idx * width], width);
    }
};