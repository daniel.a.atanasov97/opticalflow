#pragma once

#include "Array2d.hpp"

template <typename ValueType, typename SizeType>
void Conv2d(
    Array2d<ValueType, SizeType> const &image,
    Array2d<ValueType, SizeType> const &kernel,
    Array2d<ValueType, SizeType> &dest
)
{
    SizeType image_height = image.Height();
    SizeType image_width = image.Width();

    SizeType kernel_height = kernel.Height();
    SizeType kernel_width = kernel.Width();

    SizeType cy = kernel_height / 2;
    SizeType cx = kernel_width / 2;

    for (SizeType y = 0; y < image_height; y++)
    {
        for (SizeType x = 0; x < image_width; x++)
        {
            dest[y][x] = 0;
            for (SizeType ky = 0; ky < kernel_height; ky++)
            {
                SizeType dy = y + ky - cy;
                SizeType my = kernel_height - ky - 1;

                // TODO@Daniel:
                // Find a nice way to support different types of padding
                // Currently padding with zeroes

                /*while (dy < 0)
                {
                    dy += image_height;
                }

                while (dy >= image_height)
                {
                    dy -= image_height;
                }*/

                if (dy < 0 || dy >= image_height)
                {
                    continue;
                }

                for (SizeType kx = 0; kx < kernel_width; kx++)
                {
                    SizeType dx = x + kx - cx;
                    SizeType mx = kernel_width - kx - 1;

                    /*while (dx < 0)
                    {
                        dx += image_width;
                    }

                    while (dx >= image_width)
                    {
                        dx -= image_width;
                    }*/

                    if (dx < 0 || dx >= image_width)
                    {
                        continue;
                    }

                    dest[y][x] += kernel[my][mx] * image[dy][dx];
                }
            }
        }
    }
}


template <typename ValueType, typename SizeType>
Array2d<ValueType, SizeType> Conv2d(
    Array2d<ValueType, SizeType> const &image,
    Array2d<ValueType, SizeType> const &kernel
)
{
    Array2d<ValueType, SizeType> dest(image.Width(), image.Height());
    Conv2d(image, kernel, dest);
    return dest;
}
